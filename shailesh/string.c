#include <stdio.h>

int main() {
	char name[25];
	int num;
	
	printf("Please enter your name:\n");
	
	//scanf("%s",name); // it will accept the string but when you press enter key it will terminate the program
	//scanf("%[^\n]s",name); // will work in same way but won't terminate the program
	gets(name); // works fine you can use puts(name); to print name on screen
	
	printf("Please enter marks\n");
	
	scanf("%d", &num);
	
	puts(name);
	printf("%s", name);
	printf("%d", num);	
}
