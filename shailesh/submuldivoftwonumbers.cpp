//subtract, multiply, divide
#include <stdio.h>
int main(){
//first you require two variables for storing numbers (int)
//three variables are also subtraction, multiplication,division(float)
int num1,num2,subtractionanswer,multiplyanswer;
float divisionanswer;//first we declare the variables (not .2)
//how many bytes will it occupy in memory? 20 bytes
printf("Enter two numbers\n");//\n is escape sequence for new line
scanf("%d %d",&num1,&num2);//user can enter two numbers,%d not a variable
subtractionanswer=num1-num2;
printf("%d-%d= %d\n",num1,num2,subtractionanswer);//%d replaced
//\n part of string
multiplyanswer=num1*num2;//storing multiplication value in multiplyanswer variable
printf("%d*%d= %d\n",num1,num2,multiplyanswer);
divisionanswer=num1/num2;
printf("%d/%d= %.2f\n",num1,num2,divisionanswer);
//modify w/ if else statement
}
