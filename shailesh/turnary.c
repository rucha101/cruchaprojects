/* Find greater number from 2 numbers */

#include <stdio.h>

int main(){
	int x, y, z;
	
	printf("Enter 2 numbers:\n");
	
	scanf("%d %d", &x, &y);
	
	if (x > y) {
		printf("%d is greater", x);
	} else {
		printf("%d is greater", y);
	}
	
	/* turnary operator */
	
	
	// z = (if x is greater than y) ? if true then assign 10 to z : else assign 20 to z
	/*
	if (x > y) {
		z = 10;
	} else {
		z = 20;
	}
	*/
	
	z = (x > y) ? 10 : 20;
	
	printf("\nz = %d", z);
	
}
