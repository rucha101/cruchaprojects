/*

while (condition) {
	// statement
}

do {
	// perform this code at least once
	
	do you want to continue press y for yes and n for no
} while(condition);


*/

#include <stdio.h>

int main(){
	
	int x;
	
	do {
		printf("\nEnter a number\n");
		scanf("%d", &x);
		printf("you\'ve entered %d", x);
		
	} while (x > 0);
	
	printf("\nGood bye..!");
}
