/* 

switch (variable) {
	case 1:
		break;
	case 2:
		break;
	.
	.
	default:
		break;
}

*/


/* Read a number and print day of the week */

#include <stdio.h>

int main(){
	//int day_number;
	char flag;
	
	//printf("Please enter number:\n");
	
	//scanf("%d", &day_number);
	
	printf("Press y for proceed and n to exit\n");
	
	scanf("%c", &flag);
	/*
	switch (day_number) {
		case 1:
				printf("\nMonday");
			break;
		case 2:
				printf("\nTuesday");
			break;
		case 3:
				printf("\nWednesday");
			break;
		default:
				printf("\nNot a week number");
			break;
			
	}
	*/
	switch (flag){
		case 'y':
			printf("You can proceed");
			break;
		case 'n':
			printf("Good Bye");
			break;
		default:
			printf("Wrong entry");
	}

}
