/* Swap without using third variable */
#include <stdio.h>

int main() {
	int num1;
	int num2;
	
	printf("Program to swap 2 numbers without using third variable\n\n");
	printf("Please enter number 1\n");
	scanf("%d", &num1);
	printf("Please enter number 2\n");
	scanf("%d", &num2);
	
	printf("\nValues before Swapping\n");
	printf("Number 1: %d\n", num1); // 5
	printf("Number 2: %d\n\n", num2); // 6
	
	num1 = num1 + num2; // 5 + 6 = 11
	num2 = num1 - num2; // 11 - 6 = 5
	num1 = num1 - num2; // 11 - 5 = 6
	
	printf("\nValues after Swapping\n");
	printf("Number 1: %d\n", num1); // 6
	printf("Number 2: %d\n", num2); // 5
}
