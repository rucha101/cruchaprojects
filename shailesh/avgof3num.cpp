#include <stdio.h>
int main(){
	int num1, num2, num3;
	float average;	//can have any label for int datatype as long as you specify formula
					//to calculate it 
					//formula for avg is total of numbers/total numbers num1+num2+num3
		
	printf("Hello User\nProgram to calculate average\n\n");
	printf("Please enter num 1\n");
	scanf("%d",&num1);
	printf("Please enter num 2\n");
	scanf("%d",&num2);
	printf("Please enter num 3\n");
	scanf("%d",&num3);
	
	average = (num1+num2+num3) / 3.0;
	printf("The avg of 3 numbers is %.3f", average);
}
