//using the third variable
#include <stdio.h>
int main(){
	int num1, num2, num3;//declaring variables
	//one line blank
	printf("Enter number1\n");
	scanf("%d", &num1);
	printf("Enter number2\n");
	scanf("%d", &num2);
	//num1=5,num2=7,num3=0
	printf("num1=%d\nnum2=%d\n", num1, num2);
	num3 = num1;//num1=5,num2=7,num3=5
	num1 = num2;//num1=7,num2=7,num3=5
	num2 = num3;//num1=7,num2=5,num3=5
	printf("Swapped Value :\nnum1=%d\nnum2=%d\n", num1, num2);	
}

