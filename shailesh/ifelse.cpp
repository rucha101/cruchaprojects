/* Program to explain if else condition */
#include <stdio.h>
int main() {
	if(1) {
		printf("\nI am in if condition");
	} else {
		printf("\nI am in else condition");
	}
	
	if(false) {
		printf("\nI am in if condition");
	} else {
		printf("\nI am in else condition");
	}
	
	if(2 > 1) {
		printf("\n2 is greater than 1");
	} else {
		printf("\n2 is not greater than 1");
	}
}
