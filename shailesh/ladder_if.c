/*

Ladder if statement

if (condition) {
	// run if first condition is true
} else if(condition) { // if first condition is falst then  check this condition
	
} else if(condition) { // if first two condition false then it will check this condition
	// run this code
} else {
	//run if no condition matches
	// finally block run if no condition match
}

*/

/* Read percentage and print the division of marks */

#include <stdio.h>

int main(){
	
	float percentage;
	
	printf("Enter percentage\n");
	
	scanf("%f", &percentage);
	
	if(percentage >= 75) {
		printf("Passed with distinction\n");
	} else if (percentage >= 60) {
		printf("Passed");
	} else if (percentage >= 35) {
		printf("Passed in 2nd class");
	} else {
		printf("Failed");
	}
	
	
	// test cases - checking all the conditions
	
	
}
