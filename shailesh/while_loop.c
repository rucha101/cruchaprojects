/* WAP to print "C programming is fun" 5 times on screen */

#include <stdio.h>

int main(){
	
	int x = 1;
	
	printf("%d\n", x);
	
	while (x <= 5) {
		printf("%d C programming is fun\n", x);
		x++;
	}
	
	/*
	//increment x
	x++; // x = x + 1;
	
	printf("%d\n", x);
	
	//decrement x
	x--; // x = x - 1;
	
	printf("%d\n", x);
	
	
	printf("C programming is fun\n");
	printf("C programming is fun\n");
	printf("C programming is fun\n");
	printf("C programming is fun\n");
	printf("C programming is fun\n");
	*/
	
	
	
	
}

/* while loop 

syntax:

	while (condition) {
		statement..
		.
		.
		increment or degrement variable
	}
git clone https://gitlab.com/rucha101/cruchaprojects.git

*/
