/* WAP to read 2 numbers and print addition of those two numbers */

#include <stdio.h>

int main() {
	int total,num1,num2;
	
	printf("Please enter value for number 1\n");
	scanf("%d", &num1);
	
	printf("Please enter value for number 2\n");
	scanf("%d", &num2);
	
	total = num1 + num2;
	printf("%d + %d = %d", num1, num2, total);
}


