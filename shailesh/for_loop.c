/* WAP to print table of 5 */

#include <stdio.h>

int main() {
	/*
	int x = 1;
	
	while (x <= 10) {
		printf("5 X %d = %d\n", x, (x * 5));
		x++;
	}
	*/
	
	int x;
	
	for(x = 1; x <= 10; x++) {
		printf("7 X %d = %d\n", x, (x * 7));
	}
	
	printf("\nProgram ends here");
	
}

/*
int x;

for(initialization; condition; increment/decrement) {
	statement;
}
*/
