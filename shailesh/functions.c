/*

functions

{
// block of code to perform some operation, action or task
// it categorized by logics
// addition, multiplication
// it runs from stack memory
}

returnType functionName(argument1, argument2,...) { // function heading
	// function body
	
	//argument1, argument2, are available in function's block only
}

*/

#include <stdio.h>

void add(int x, int y) {
	int total;
	total = x + y;
	printf("\n%d + %d = %d", x, y, total);
}

int sub(int x, int y){
	int subraction;
	subraction = x - y;
	return subraction;
}

int oddNumber(int num) {
	if(num%2 == 0){
		// even
		return 0;
	} else {
		// odd
		return 1;
	}
}

int main(){
	add(5,6);
	add(7,8);
	add(30,70);
	
	int z = 100 + sub(10,7); // function in expression
//	int y = 100 + add(5,7); // void function 
	
	printf("\n subtraction: %d", z);
	
	int number = 10;
	 
	printf("\n%d", oddNumber(number));
	
	if(oddNumber(number)){
		printf("\n Number is odd");
	} else {
		printf("\n Number is even");
	}
}
