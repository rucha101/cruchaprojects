//WAP to read a number and print square of that number
// use do while loop to terminate the program

#include <stdio.h>
int main(){
int x;char flag;
	
  	do {
  		printf("Please enter number to find square\n");
		scanf("%d",&x);
		printf("Square of %d = %d\n",x,x*x);
		printf("To continue press y if not press n\n");
		scanf("%c",&flag);
	 }while (flag == 'y');
	 
	 printf("Goodbye.");
	 
}
