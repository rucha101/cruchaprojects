#include <stdio.h>
#include <string.h>



struct Student
{
	char name[20];
	int age;
	int roll_no;
};


union Bank
{
	int amount;
	int balance;
	double tax;
};

int main() {
	/*
	struct Student s;
	//strcpy(s.name, "Shailesh Sonare");
	gets(s.name);
	printf("\nName: %s\n", s.name);
	printf("%u", sizeof(s));
	scanf("%d", &s.age);
	union Bank b[5];
	printf("%u", sizeof(b));
	
	b[6].balance = 25000;
	b[0].balance = 50000;
	
	printf("\n%d", b[6].balance);
	printf("\n%d", b[0].balance);
	*/
	int x = 25;
	int *p = &x;
	
	printf("\n%d", x);
	printf("\n%d", *p);
	printf("\n%u", p);
	printf("\n%d", *&x);
	
	char *name = "Shailesh Sonare";
	a:printf("%s\n", name);
	b:printf("\n%u", *name);
	c:printf("\n%u", sizeof(p));
	
	char ch;
	d:printf("\nEnter char");
	scanf(" %c", &ch);
	
	if(ch == 'a'){
		goto a;
	} else if(ch == 'b'){
		goto b;
	} else if(ch == 'c'){
		goto c;
	} else if(ch == '0'){
		goto q;
	} else {
		goto d;
	}
	
	q:printf("cya");
}
