#include <stdio.h>

int main() {
	
/*
declare 5 subjects(int),total(int), and percentage(float)
variable name start w/ letters instead of numbers
*/

	int sub1, sub2, sub3, sub4, sub5, total;
	float percentage;

	printf("Hello User\nPlease Enter marks of 5 subjects\n");
	scanf("%d %d %d %d %d", &sub1, &sub2, &sub3, &sub4, &sub5);

	total = sub1 + sub2 + sub3 + sub4 + sub5;
	percentage = (total/500.0) * 100;
	
	printf("You've got %d marks out of %d\n", total, 500);
	printf("And Your percentage is %.2f", percentage);

}
